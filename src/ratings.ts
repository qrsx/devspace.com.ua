import {setUserAgentClassNames} from "./browser/navigator";
import dataProvider from "./core/fetch";
import unmarshal from "./core/zip";
import {companyUrl} from "./core/format"
import {salaryVacancyCount} from "./core/entities";

const body = document.getElementById("body");

setUserAgentClassNames(body);

function initOnce(source: Array<any>) {
    const companies = unmarshal(source);

    const length = companies.length;

    const views = new Array(length);

    for (let i = 0; i < length; i++) {
        const company = companies[i];
        const url = companyUrl(company.alias);

        views[i] = `<tr><td align="center">${i + 1}</td><td><a href="${url}" target="_blank">${company.name}</a></td><td align="center">${salaryVacancyCount(company.vacancies)}</td><td align="center">${company.vacancies.length}</td><td align="center">${company.review_count}</td><td align="center">${company.photoExists ? `<a href="${url}/photos/" target="_blank">фотографії</a>` : ""}</td></tr>`;
    }

    document.getElementById("js-companies").innerHTML = views.join("");
}

dataProvider(initOnce, console.error);