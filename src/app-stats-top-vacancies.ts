import dataProvider from "./core/fetch";
import unmarshal from "./core/zip";

import hljs from "@highlightjs/highlight.js/lib/core";
import json from "@highlightjs/highlight.js/lib/languages/json";
import {Company} from "./core/entities";

class Vacancy {
    constructor(public title: string, public count: number) {
    }
}

function vacancies(companies: Array<Company>): Array<Vacancy> {
    const map: { [key: string]: Vacancy; } = {};

    const result = new Array<Vacancy>();

    for (let company of companies) {
        for (let vacancy of company.vacancies) {
            const title = vacancy.title.toLowerCase();

            if (map.hasOwnProperty(title)) {
                map[title].count += 1
            } else {
                const vc = new Vacancy(vacancy.title, 1);

                result.push(vc);

                map[title] = vc;
            }
        }
    }

    result.sort(function (a, b) {
        return b.count - a.count;
    });

    return top(result, 3);
}

function top(source: Array<Vacancy>, count: number): Array<Vacancy> {
    const result = new Array<Vacancy>();

    for (let vacancy of source) {
        if (vacancy.count < count) {
            break;
        }

        result.push(vacancy);
    }

    return result;
}

dataProvider(function (source: Array<any>) {
    const companies = unmarshal(source);

    const $output = document.getElementById("output");

    hljs.registerLanguage("json", json);

    $output.innerHTML = hljs.highlight("json", JSON.stringify(vacancies(companies), null, 4)).value;
}, console.error)
